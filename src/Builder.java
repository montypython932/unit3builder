import java.util.Scanner;
public class Builder {

	static StringBuilder sb = new StringBuilder("");
	
	public static void main(String[] args) {
		getFirstInput();
		sb.append("I love it!");
		getSecondInput();
		System.out.println(sb);
	}
	
	//gets first input "Java is fun!" from user and appends it to sb
	public static void getFirstInput() {
		Scanner input = new Scanner(System.in);
		String text;
		for (;;) {
			System.out.println("Please type in 'Java is fun!'");
			text = input.nextLine();
			if (text.equals("Java is fun!")) {
				sb.append(text);
				break;
			}
		}
	}
	
	//gets second input from user "Yes, " and inserts it into sb
	public static void getSecondInput() {
		Scanner input = new Scanner(System.in);
		String text;
		for (;;) {
			System.out.println("Please type in 'Yes, ");
			text = input.nextLine();
			if (text.equals("Yes, ")) {
				sb.insert(12, text);
				break;
			}
		}
	}
}
